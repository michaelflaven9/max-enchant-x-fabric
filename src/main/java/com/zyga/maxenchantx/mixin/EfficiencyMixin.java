package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.EfficiencyEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(EfficiencyEnchantment.class)
public class EfficiencyMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
