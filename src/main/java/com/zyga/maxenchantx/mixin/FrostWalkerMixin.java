package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.FrostWalkerEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(FrostWalkerEnchantment.class)
public class FrostWalkerMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }

    @Overwrite
    public boolean isTreasure() {
        return false;
    }
}
