package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.KnockbackEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(KnockbackEnchantment.class)
public class KnockbackMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
