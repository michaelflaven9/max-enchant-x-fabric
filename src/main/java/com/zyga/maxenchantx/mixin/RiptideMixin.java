package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.RiptideEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(RiptideEnchantment.class)
public class RiptideMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
