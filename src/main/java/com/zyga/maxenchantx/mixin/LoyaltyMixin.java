package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.LoyaltyEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(LoyaltyEnchantment.class)
public class LoyaltyMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
