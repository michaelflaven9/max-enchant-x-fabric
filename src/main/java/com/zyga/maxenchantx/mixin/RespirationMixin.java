package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.RespirationEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(RespirationEnchantment.class)
public class RespirationMixin {

    @Overwrite
    public int getMaxLevel() {
        return 10;
    }
}
